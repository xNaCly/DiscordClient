const { app, BrowserWindow } = require("electron");
function createWindow() {
	let win = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			nodeIntegration: true,
			devTools: true
		},
		icon: "./pictures/app.ico",
		// frame: false,
		opacity: 1.0
	});
	win.loadFile("index.html");
}
app.on("ready", createWindow);
