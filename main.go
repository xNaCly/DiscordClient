package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"net"
	"net/http"
	"os"
	"path/filepath"

	"github.com/zserge/webview"
)


func startServer() string {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		defer ln.Close()
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			path := r.URL.Path
			if len(path) > 0 && path[0] == '/' {
				path = path[1:]
			}
			if path == "" {
				path = "dist/index.html"
			}
			if path == "main.js" {
				path = "dist/main.js"
			}
			bs, err := os.Open(path);
			bs2, err := ioutil.ReadAll(bs)
			if ; err != nil {
				w.WriteHeader(http.StatusNotFound)
			} else {
				w.Header().Add("Content-Type", mime.TypeByExtension(filepath.Ext(path)))
				io.Copy(w, bytes.NewBuffer(bs2))
			}
		})
		log.Fatal(http.Serve(ln, nil))
	}()
	return "http://" + ln.Addr().String()
}


func main() {
	url := startServer()
	w := webview.New(webview.Settings{
		Width:  800,
		Height: 600,
		Title:  "Todo App",
		URL:    url,
		Resizable: true,
	})
	defer w.Exit()
	w.Run()
}
