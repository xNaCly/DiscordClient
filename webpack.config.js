const path = require('path');

module.exports = {
	entry: './content.js',
	mode: 'production',
	node: {
		fs: 'empty',
		net: 'empty'
	},
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'dist')
	}
};
