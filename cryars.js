// const requestLib = require('request');
// const DiscordSocket = require('./socket');

window.request = (url, params, callback) => {
	var body = '';
	var response = {};
	var error;

	fetch(url, params)
		.then(res => {
			response = res;
			return res.text();
		})
		.then(text => {
			body = text;
		})
		.catch(e => {
			error = e;
		})
		.finally(() => {
			callback(error, response, body);
		});
};

class Client {
	constructor(Token, settings) {
		this.Token = Token;

		if (settings && typeof settings == 'object') {
			if (settings.proxy) {
				var proxy = settings.proxy;
			}
		}
	}

	gateway_connect() {}

	check_token() {
		return new Promise((resolve, reject) => {
			let url = 'https://discordapp.com/api/v6/channels/457229726659117067';
			// console.log(url, this.Token);
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					// console.log(response, error, body);
					switch (response.status) {
						case 404:
						case 200:
							return resolve(true);
						case 401:
							return reject('invalid token');
						case 403:
							return reject('unverifed and flaged token');
					}
				}.bind(this)
			);
		});
	}

	get_user_info() {
		return new Promise((resolve, reject) => {
			let url = 'https://discordapp.com/api/v6/users/@me';
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					return resolve(JSON.parse(body));
				}
			);
		});
	}

	get_guilds() {
		return new Promise(y => {
			request(
				'https://discordapp.com/api/users/@me/guilds',
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					body = JSON.parse(body);
					var guilds = body.map(guild => {
						return {
							name: guild.name,
							id: guild.id,
							icon:
								guild.icon != null
									? `https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.png?size=128`
									: '3fb5f8de75f22494de95e5c1d47af51'
						};
					});
					y(guilds);
				}
			);
		});
	}

	get_guild_id_by_name(guild_name) {
		return new Promise((resolve, reject) => {
			if (!guild_name)
				return reject(
					'Fuck, pls give me smth to work with bitch | ! Error: No NAME parameter for get_guild_id_by_name specified !'
				);
			return resolve(this.get_guilds().then(y => y.find(x => x.name === guild_name)));
		});
	}

	/**
	 * @param {string} guild_id id of the guild
	 **/
	guild(guild_id) {
		return new Promise((resolve, reject) => {
			if (!guild_id)
				return reject(
					'Fuck, pls give me smth to work with bitch | ! Error: No NAME parameter for get_guild_id_by_name specified !'
				);
			let url = `https://discordapp.com/api/v6/guilds/${guild_id}`;
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					body = JSON.parse(body);
					return resolve(body);
				}
			);
		});
	}

	set_guild(Guild_invite) {
		// ! only for User accounts
		let url = `https://discordapp.com/api/v6/invites/${Guild_invite}`;
		request(url, {
			method: 'POST',
			headers: { authorization: this.Token }
		});
	}

	async set_guilds(Guild_invite) {
		if (Guild_invite.length > 10) {
			console.error('Too many invites, the bot may be banned for joining to many guilds');
		}
		var x;
		for (x of Guild_invite) {
			sleep(5000);
			let url = `https://discordapp.com/api/v6/invites/${x}`;
			await request(url, {
				method: 'POST',
				headers: { authorization: this.Token }
			});
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {}
			);
		}
	}

	set_house(house_id) {
		let url = `https://discordapp.com/api/v6/hypesquad/online`;

		// house set:
		// id: 1 = house of bravery
		// id: 2 = house of brilliance
		// id: 3 = house of ballance
		// https://discordapp.com/api/v6/hypesquad/online

		request(url, {
			method: 'POST',
			headers: {
				authorization: this.Token,
				'content-type': 'application/json'
			},
			body: JSON.stringify({ house_id })
		});
	}

	send_fr(username_discriminator) {
		let url = 'https://discordapp.com/api/v6/users/@me/relationships';
		username_discriminator = username_discriminator.split('#');
		let username = username_discriminator[0];
		let discriminator = username_discriminator[1];
		request(url, {
			method: 'POST',
			headers: {
				authorization: this.Token,
				'content-type': 'application/json'
			},
			body: JSON.stringify({ username: username, discriminator: discriminator })
		});
	}

	send_message(channel_id, message) {
		let url = `https://discordapp.com/api/v6/channels/${channel_id}/messages`;
		request(
			url,
			{
				method: 'POST',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				},
				body: JSON.stringify({
					content: message,
					tts: false
				})
			},
			function(error, response, body) {}
		);
	}

	/**
	 * @param {string} name Name of the guild
	 * @param {string} region Serverregion of the guild [Europe, Brazil, Russia]
	 **/
	create_guild(name, region, icon) {
		let url = 'https://discordapp.com/api/v6/guilds';

		if (!region) region = 'europe';
		if (!icon) icon = 'null';
		request(
			url,
			{
				method: 'POST',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				},
				body: JSON.stringify({
					name: name,
					region: region.toLowerCase(),
					icon: icon
				})
			},
			function(error, response, body) {}
		);
	}

	create_guild_invite(channel_id) {
		let url = `https://discordapp.com/api/v6/channels/${channel_id}/invites`;
		request(
			url,
			{
				method: 'POST',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				},
				body: JSON.stringify({
					validate: null,
					max_age: 0,
					max_uses: 0,
					target_user_type: null,
					temporary: false
				})
			},
			function(error, response, body) {
				body = JSON.parse(body);
				console.log('https://discord.gg/' + body.code);
			}
		);
	}

	/**
	 * @param {string} channel_id Id of the channel
	 * @param {string} new_name New Name of the channel
	 * @param {boolean} nsfw Nsfw true or false
	 **/
	edit_channel(channel_id, new_name, nsfw) {
		let url = `https://discordapp.com/api/v6/channels/${channel_id}`;
		request(
			url,
			{
				method: 'PATCH',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				},
				body: JSON.stringify({
					name: new_name,
					type: 0,
					topic: '',
					bitrate: 64000,
					user_limit: 0,
					nsfw: nsfw,
					rate_limit_per_user: 0
				})
			},
			function(error, response, body) {
				// console.log(response.status);
				body = JSON.parse(body);
			}
		);
	}

	/**
	 * @param {string} guild_id Id of the guild
	 * @param {string} name Name of the channel
	 **/
	create_channel(guild_id, name) {
		// POST
		// `https://discordapp.com/api/v6/guilds/{guild_id}/channels`
		let url = `https://discordapp.com/api/v6/guilds/${guild_id}/channels`;
		// {"type":0,"name":name,"permission_overwrites":[],"parent_id":""}
		request(
			url,
			{
				method: 'POST',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				},
				body: JSON.stringify({
					type: 0,
					name: name,
					permission_overwrites: []
				})
			},
			function(error, response, body) {
				// console.log(response.status);
				// body = JSON.parse(body);
				// console.log(body);
			}
		);
	}

	delete_channel(channel_id) {
		let url = `https://discordapp.com/api/v6/channels/${channel_id}`;
		request(
			url,
			{
				method: 'DELETE',
				headers: {
					authorization: this.Token,
					'content-type': 'application/json'
				}
				// body: JSON.stringify({
				//   type: 0,
				//   name: name,
				//   permission_overwrites: []
				// })
			},
			function(error, response, body) {
				// console.log(response.status);
				// body = JSON.parse(body);
				// console.log(body);
			}
		);
	}

	get_channels(guild_id) {
		return new Promise((resolve, reject) => {
			if (!guild_id)
				return reject(
					'Fuck, pls give me smth to work with bitch | ! Error: No NAME parameter for get_guild_id_by_name specified !'
				);
			let url = `https://discordapp.com/api/v6/guilds/${guild_id}/channels`;
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					body = JSON.parse(body);
					return resolve(body);
				}
			);
		});
	}

	get_roles(guild_id) {
		return new Promise((resolve, reject) => {
			if (!guild_id)
				return reject(
					'Fuck, pls give me smth to work with bitch | ! Error: No NAME parameter for get_guild_id_by_name specified !'
				);
			let url = `https://discordapp.com/api/v6/guilds/${guild_id}/roles`;
			request(
				url,
				{
					method: 'GET',
					headers: { authorization: this.Token }
				},
				function(error, response, body) {
					body = JSON.parse(body);
					return resolve(body);
				}
			);
		});
	}

	del_role(guild_id, role_id) {
		let url = `https://discordapp.com/api/v6/guilds/${guild_id}/roles/${role_id}`;
		request(
			url,
			{
				method: 'DELETE',
				headers: { authorization: this.Token }
			},
			function(error, response, body) {}
		);
	}

	ban_member(guild_id, member_id, reason) {
		let url = `https://discordapp.com/api/v6/guilds/${guild_id}/bans/${member_id}?delete-message-days=1&reason=`;
		// if (reason) {
		//   url + `&reason=${reason}`;
		// }
		request(
			url,
			{
				method: 'PUT',
				headers: { authorization: this.Token }
			},
			function(error, response, body) {
				console.log(response.status);
			}
		);

		// https://discordapp.com/api/v6/guilds/${guild_id}/bans/${member_id}?delete-message-days=1}
	}

	async mass_create_channels(guild_id, name, amount) {
		for (var x = 0; x < amount; x++) {
			await this.create_channel(guild_id, name);
		}
	}

	async mass_delete_channels(guild_id) {
		let channels = await this.get_channels(guild_id);
		channels.forEach(async element => {
			await this.delete_channel(element.id);
		});
	}

	async mass_msg_create(guild_id, message_content) {
		let channels = await this.get_channels(guild_id);
		for (var i = 0; i < channels.length; i++) {
			let channel = channels[i];
			await this.send_message(channel.id, message_content);
			await sleep(400);
		}
	}

	async mass_del_roles(guild_id) {
		let roles = await this.get_roles(guild_id);
		roles.forEach(role => {
			this.del_role(guild_id, role.id);
			sleep(200);
		});
	}

	async wizz_server(guild_id, channel_names, channel_amount, message_content) {
		await this.mass_delete_channels(guild_id);
		if (channel_names && channel_amount) {
			await this.mass_create_channels(guild_id, channel_names, channel_amount);
			await sleep(500);
		}
		if (message_content) {
			await this.mass_msg_create(guild_id, message_content);
			await sleep(500);
		}
		await this.mass_del_roles(guild_id);
	}
	async raid_server(guild_id, message_content) {
		await this.mass_msg_create(guild_id, message_content);
	}
}
module.exports = Client;

function sleep(ms) {
	return new Promise(res => {
		setTimeout(() => {
			res();
		}, ms);
	});
}
