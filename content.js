console.log('reloaded' + Math.random());
const cry = require('./cryars');
var client;

async function tokenCheckSubmit() {
	let token = document.querySelector('#token_input').value;
	let bot = $('#botcheck').prop('checked');
	token = bot ? 'Bot ' + token : token;

	client = new cry(token);
	client.gateway_connect();
	let $$ = $('#error');
	try {
		await client.check_token();
		$$.addClass('hidden');
		let user = await client.get_user_info();
		$('#username').val(user.username + '#' + user.discriminator);
		$('#userid').val(user.id);
		$('#email').val(user.email);
		$('#locale').val(user.locale);
		$('#success').removeClass('hidden');
	} catch (error) {
		$$.removeClass('hidden');
		$$.html(error);
		$('#success').addClass('hidden');
	}
}

async function joinGuild() {
	let invite = document.querySelector('#invite_input').value;
	if (!invite.length == 6) {
		console.log('invite not long enough °-°');
		//errormsg here
	}
	// NjQ4NjIzNzc5NzE0NDMzMDQ5.Xdw8IQ.o_xI_sbKWfbElhldvZ9KROtGPQQ
	await client.set_guild(invite);
}

async function sendMessage() {
	let msg = document.querySelector('#msg_content_input').value;
	let id = document.querySelector('#id_input').value;
	if (!msg) return; //errormsg here
	await client.raid_server(id, msg);
}

async function sendfriendreq() {
	let friend = document.querySelector('#friend_input').value;
	await client.send_fr(friend);
}

function resizable(el, factor) {
	var int = Number(factor) || 7.7;
	function resize() {
		el.style.width = (el.value.length + 1) * int + 'px';
	}
	var e = 'keyup,keypress,focus,blur,change'.split(',');
	for (var i in e) el.addEventListener(e[i], resize, false);
	resize();
}

function close_exit() {
	// const remote = require('electron').remote;
	// let w = remote.getCurrentWindow();
	// w.close();
}

document.addEventListener('DOMContentLoaded', function() {
	resizable(document.querySelector('#token_input'), 10);
});

window.close_exit = close_exit;
window.tokenCheckSubmit = tokenCheckSubmit;
window.sendMessage = sendMessage;
window.sendfriendreq = sendfriendreq;
window.resizable = resizable;
